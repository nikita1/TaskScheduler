package com.example.rbsl.spnablestring;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

public class noAction extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        // TODO: This method is called when the BroadcastReceiver is receiving
        // an Intent broadcast.
        if(intent.getAction().equals("notification"))
        {
            Bundle answerBundle = intent.getExtras();
            int userAnswer = answerBundle.getInt("userAnswer");
            if(userAnswer == 1) {
                Log.v("notificationButton","Yes"+answerBundle.getString("message"));

            }else if(userAnswer == 2) {
                Log.v("notificationButton","No"+answerBundle.getString("message"));
            }
        }
    }
}
