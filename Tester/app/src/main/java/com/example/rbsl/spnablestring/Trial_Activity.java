package com.example.rbsl.spnablestring;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.TimePicker;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class Trial_Activity extends AppCompatActivity {
    Button btnAddTask,btnviewlist;
    ImageButton btnsettime;
    TimePickerDialog.OnTimeSetListener time;
    Calendar myCalendar;
    TextView edttime;
        static ArrayList<Tasklist> taskarraylist = new ArrayList<>();
     int  mHour, mMinute;
    Context context;
    EditText edittaskname;
    static List<Tasklist> tasklist= new ArrayList<Tasklist>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        myCalendar = Calendar.getInstance();
        setContentView(R.layout.activity_trial_);
        edittaskname = (EditText)findViewById(R.id.edittaskname);
        edttime = (TextView) findViewById(R.id.edttime);
        btnAddTask = (Button) findViewById(R.id.btn_add_task);
        btnsettime = (ImageButton) findViewById(R.id.btntimeset);
        btnviewlist = (Button)findViewById(R.id.btnviewlist);

        btnAddTask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               Tasklist tasklist = new Tasklist(edittaskname.getText().toString(),true,edttime.getText().toString());
               DatabaseHandling databaseHandling= new DatabaseHandling(Trial_Activity.this);
                databaseHandling.addUsers(tasklist);
               taskarraylist = (ArrayList<Tasklist>) databaseHandling.getListofallClients();




            }
        });
        btnsettime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                    // Get Current Time
                    final Calendar c = Calendar.getInstance();
                    mHour = c.get(Calendar.HOUR);
                    mMinute = c.get(Calendar.MINUTE);


                    // Launch Time Picker Dialog
                    TimePickerDialog timePickerDialog = new TimePickerDialog(Trial_Activity.this,
                            new TimePickerDialog.OnTimeSetListener() {

                                @Override
                                public void onTimeSet(TimePicker view, int hourOfDay,
                                                      int minute) {
                                    Calendar calendar = Calendar.getInstance();
                                    calendar.set(0, 0, 0, hourOfDay, minute);
                                    edttime.setText((String) DateFormat.format("hh:mm aaa", calendar));
                                }
                            }, mHour, mMinute, false);
                    timePickerDialog.show();

            }
        });
        btnviewlist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Trial_Activity.this,TaskList.class);
                intent.putExtra("mylist",taskarraylist);
                startActivity(intent);

            }
        });
    }

}