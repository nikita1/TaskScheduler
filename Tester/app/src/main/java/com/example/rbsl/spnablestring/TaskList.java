package com.example.rbsl.spnablestring;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

import java.util.ArrayList;

public class TaskList extends AppCompatActivity {
ListView tasklist;
   ArrayList<Tasklist>taskarraylist = new ArrayList<>();
    TasklistAdapter tasklistAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task_list);
        DatabaseHandling databaseHandling= new DatabaseHandling(TaskList.this);
        taskarraylist = (ArrayList<Tasklist>) databaseHandling.getListofallClients();
        tasklist = (ListView)findViewById(R.id.lvtasklist);
        tasklistAdapter = new TasklistAdapter(TaskList.this, R.layout.listdesign, taskarraylist);
        tasklist.setAdapter(tasklistAdapter);
        tasklistAdapter.setNotifyOnChange(true);

    }
}
