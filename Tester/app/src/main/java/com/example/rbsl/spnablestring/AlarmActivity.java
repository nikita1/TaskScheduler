package com.example.rbsl.spnablestring;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;

import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.ToggleButton;

import java.util.Calendar;

public class AlarmActivity extends AppCompatActivity  {

    AlarmManager alarmManager;
    private PendingIntent pendingIntent;
    private TimePicker alarmTimePicker;
    private static AlarmActivity inst;
    private TextView alarmTextView;
    ToggleButton alarmToggle;
    EditText edt_taskname;
    Button btnsendnotification;

    String taskmessage;
    long requestCode;
    private NotificationManager alarmNotificationManager;
    public static AlarmActivity instance() {
        return inst;
    }

    @Override
    public void onStart() {
        super.onStart();
        inst = this;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alarm);
        edt_taskname = (EditText)findViewById(R.id.taskName);
        alarmTimePicker = (TimePicker) findViewById(R.id.alarmTimePicker);
        alarmTextView = (TextView) findViewById(R.id.alarmText);
        alarmToggle = (ToggleButton) findViewById(R.id.alarmToggle);
        alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
        btnsendnotification = (Button)findViewById(R.id.send_notification);
        btnsendnotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getBaseContext(),TaskCreaterActivity.class);
                startActivity(intent);

                alarmNotificationManager = (NotificationManager) AlarmActivity.this.getSystemService(Context.NOTIFICATION_SERVICE);
                Intent homeIntent = new Intent();
                homeIntent.setAction("notification");
                Bundle yesBundle = new Bundle();
                yesBundle.putInt("userAnswer", 1);
                yesBundle.putString("message",edt_taskname.getText().toString());
                homeIntent.putExtras(yesBundle);
                PendingIntent pendinghomeIntent = PendingIntent.getBroadcast(getBaseContext(), 0, homeIntent, PendingIntent.FLAG_UPDATE_CURRENT);
                Intent recentAppIntent = new Intent(Context.NOTIFICATION_SERVICE);
                recentAppIntent.setAction("notification");
                Bundle recentAppBundle = new Bundle();
                recentAppBundle.putInt("userAnswer", 2);
                recentAppBundle.putString("message",edt_taskname.getText().toString());
                recentAppIntent.putExtras(recentAppBundle);
                PendingIntent pendingrecentAppIntent = PendingIntent.getBroadcast(getBaseContext(), 1, recentAppIntent, PendingIntent.FLAG_UPDATE_CURRENT);
                NotificationCompat.Builder alamNotificationBuilder = new NotificationCompat.Builder(AlarmActivity.this).setContentTitle("abc").setSmallIcon(R.drawable.bell)
                        .setStyle(new NotificationCompat.BigTextStyle().bigText(edt_taskname.getText().toString()))
                        .addAction(R.drawable.logo, "YES", pendinghomeIntent)
                        .addAction(R.drawable.bell, "No", pendingrecentAppIntent)
                        .setContentText(edt_taskname.getText());
                alarmNotificationManager.notify(1, alamNotificationBuilder.build());
                Log.d("AlarmService", "Notification sent.");

            }
        });
      //  taskmessage = getIntent().getExtras().getString("TaskName").toString();
       // requestCode = getIntent().getExtras().getLong("RequestCode");
    }

    public void onToggleClicked(View view) {
        if (alarmToggle.isChecked()) {
            Log.d("MyActivity", "Alarm On");
            Calendar calendar = Calendar.getInstance();
            calendar.set(Calendar.HOUR_OF_DAY, alarmTimePicker.getCurrentHour());
            calendar.set(Calendar.MINUTE, alarmTimePicker.getCurrentMinute());
            Intent myIntent = new Intent(AlarmActivity.this, AlarmReceiver.class);
            myIntent.putExtra("message",taskmessage);
            myIntent.putExtra("requestCode",requestCode);
            pendingIntent = PendingIntent.getBroadcast(AlarmActivity.this, (int)(requestCode), myIntent, 0);
            alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(),
                    1000 * 60 * (60*12), pendingIntent);

        } else {
            alarmManager.cancel(pendingIntent);
            AlarmReceiver.ringtone.stop();
            setAlarmText("");
            Log.d("MyActivity", "Alarm Off");
        }
    }

    public void setAlarmText(String alarmText) {
        alarmTextView.setText(alarmText);
    }
}

