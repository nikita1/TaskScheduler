package com.example.rbsl.spnablestring;

import android.app.Activity;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.util.Log;

import com.example.rbsl.spnablestring.AlarmActivity;
import com.example.rbsl.spnablestring.AlarmService;

/**
 * Created by rbsl on 19/8/17.
 */

public class AlarmReceiver extends WakefulBroadcastReceiver {

    private NotificationManager alarmNotificationManager;


    static Ringtone ringtone;
    @Override
    public void onReceive( Context context, Intent intent) {
        //this will update the UI with message
        AlarmActivity inst = AlarmActivity.instance();
        inst.setAlarmText("Alarm! Wake up! Wake up!");


        //this will sound the alarm tone
        //this will sound the alarm once, if you wish to
        //raise alarm in loop continuously then use MediaPlayer and setLooping(true)
        Uri alarmUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        Ringtone ringtone = RingtoneManager.getRingtone(context, alarmUri);
            alarmUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        ringtone.play();

        alarmNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);


        PendingIntent contentIntent = PendingIntent.getActivity(context,(int)(intent.getExtras().getLong("requestCode")),
                new Intent(context,AlarmActivity.class), 0);

        NotificationCompat.Builder alamNotificationBuilder = new NotificationCompat.Builder(
                context).setContentTitle("Alarm").setSmallIcon(R.drawable.bell)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(intent.getExtras().getString("message").toString()))
                .addAction(R.drawable.logo, "YES", contentIntent)
                .addAction(R.drawable.bell, "No", contentIntent)
                .setContentText(intent.getExtras().getString("message").toString());


        alamNotificationBuilder.setContentIntent(contentIntent);
        alarmNotificationManager.notify((int)(intent.getExtras().getLong("requestCode")), alamNotificationBuilder.build());
        Log.d("AlarmService", "Notification sent.");
        //this will send a notification message
       /* ComponentName comp = new ComponentName(context.getPackageName(),AlarmService.class.getName());
        startWakefulService(context, (intent.setComponent(comp)));
        setResultCode(Activity.RESULT_OK);*/
    }
}