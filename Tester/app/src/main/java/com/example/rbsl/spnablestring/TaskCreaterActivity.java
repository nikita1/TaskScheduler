package com.example.rbsl.spnablestring;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class TaskCreaterActivity extends AppCompatActivity implements View.OnClickListener {
    Button submitTaskCreatorbutton, createNewTaskCreatorbutton;
    EditText taskCreatorEdittext,requestCodeEdittext;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task_creater);
        submitTaskCreatorbutton = (Button) findViewById(R.id.taskCreatorbutton);
        createNewTaskCreatorbutton = (Button) findViewById(R.id.newtaskCreatorbutton);
        taskCreatorEdittext =(EditText)findViewById(R.id.taskCreatorEdittext);
        submitTaskCreatorbutton.setOnClickListener(this);
        createNewTaskCreatorbutton.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.taskCreatorbutton:
                Intent intent = new Intent(this,AlarmActivity.class);
                long requestCode = (int)(System.currentTimeMillis());
                intent.putExtra("TaskName",taskCreatorEdittext.getText().toString());
                intent.putExtra("RequestCode",requestCode);
                startActivity(intent);
                break;

            case R.id.newtaskCreatorbutton:
                break;
        }

    }
}
