package com.example.rbsl.spnablestring;

import android.app.Activity;
import android.app.TimePickerDialog;
import android.content.Context;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by rbsl on 22/8/17.
 */

public class TasklistAdapter extends ArrayAdapter<Tasklist> {
    Context context;
    int  mHour, mMinute;
    static final String TAG = "inAdapter";
    String countryCode;
    int layoutResourceId;
    String temp_country_code;
    ArrayList<Tasklist> data = new ArrayList<Tasklist>();
    public TasklistAdapter(Context context, int layoutResourceId, ArrayList<Tasklist> data) {
        super(context, layoutResourceId, data);
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.data = data;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        UserHolder holder = null;
        if (row == null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);
            holder = new UserHolder();
            holder.txttasktime = (TextView) row.findViewById(R.id.tasktime);
            holder.txttaskname = (TextView) row.findViewById(R.id.taskname);
           holder.btnswitch = (Switch)row.findViewById(R.id.on_off_switch);
            row.setTag(holder);
        } else {
            holder = (UserHolder) row.getTag();
        }
        final Tasklist taskDetail = data.get(position);
        holder.txttaskname.setText(taskDetail.getTask_name());
        holder.txttasktime.setText(taskDetail.getTime());
        holder.btnswitch.setChecked(taskDetail.getIsenable());



        final UserHolder finalHolder = holder;
        holder.txttasktime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                final Calendar c = Calendar.getInstance();
                mHour = c.get(Calendar.HOUR);
                mMinute = c.get(Calendar.MINUTE);


                // Launch Time Picker Dialog
                TimePickerDialog timePickerDialog = new TimePickerDialog(context,
                        new TimePickerDialog.OnTimeSetListener() {

                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay,
                                                  int minute) {
                                Calendar calendar = Calendar.getInstance();
                                calendar.set(0, 0, 0, hourOfDay, minute);
                                finalHolder.txttasktime.setText((String) DateFormat.format("hh:mm aaa", calendar));
                            }
                        }, mHour, mMinute, false);
                timePickerDialog.show();

            }
        });
        holder.btnswitch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {


                    Tasklist tasklist = new Tasklist(finalHolder.txttaskname.getText().toString(),finalHolder.btnswitch.isChecked(),finalHolder.txttasktime.getText().toString());

                DatabaseHandling databaseHandling = new DatabaseHandling(context);
                databaseHandling.update(tasklist,String.valueOf(taskDetail.getId()));

            }
        });
        return row;

    }


    static class UserHolder {
        TextView txttaskname;
        TextView txttasktime;
        Switch btnswitch;


    }
}
