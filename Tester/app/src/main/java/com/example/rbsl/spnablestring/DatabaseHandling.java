package com.example.rbsl.spnablestring;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by rbsl on 23/8/17.
 */


public class DatabaseHandling extends SQLiteOpenHelper {

    // All Static variables
    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "invoicingdatabases";

        // products table name
    private static final String Productdetails = "scheduleTask";
    private static final String TAG = "Insert query";
    // Clients Table Columns names

    private static final String KEY_PRODUCT_ID = "task_id";
    private static final String KEY_PRODUCT_NAME = "task_name";
    private static final String KEY_PRODUCT_AVAILABILITY = "isTask_available";
    private static final String KEY_PRODUCT_TIME = "task_time";

    SQLiteDatabase db;



    String CREATE_PRODUCT_TABLE = "CREATE TABLE " + Productdetails + "("
            + KEY_PRODUCT_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + KEY_PRODUCT_NAME + " TEXT,"
             + KEY_PRODUCT_AVAILABILITY + " BOOLEAN,"+KEY_PRODUCT_TIME+" TEXT)";

    public DatabaseHandling(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        db = getWritableDatabase();

    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL(CREATE_PRODUCT_TABLE);
    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + Productdetails);
        // Create tables again
        onCreate(db);
    }

    //IT IS FOR INSERTING THE USER IN THE DATABASE
    public DatabaseHandling addUsers(Tasklist tasklist) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(KEY_PRODUCT_NAME, tasklist.getTask_name());
        values.put(KEY_PRODUCT_AVAILABILITY, tasklist.getIsenable());
        values.put(KEY_PRODUCT_TIME,tasklist.getTime());
        // Inserting Row
        db.insert(Productdetails, null, values);
        db.close();
        // Closing database connection
        return  this;
    }

    public List<Tasklist> getListofallClients() {
        List<Tasklist> tasklistList = new ArrayList<Tasklist>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + Productdetails;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Tasklist tasklist = new Tasklist();
                tasklist.setId(cursor.getInt(0));
                tasklist.setTask_name(cursor.getString(1));
                tasklist.setIsenable(cursor.getString(2).equals("1")?true:false);
                tasklist.setTime(cursor.getString(3));
                // Adding contact to list
                tasklistList.add(tasklist);
            } while (cursor.moveToNext());
        }
        // return contact list
        return tasklistList;
    }

    public int update(Tasklist tasklist, String updatingId) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_PRODUCT_NAME, tasklist.getTask_name());
        values.put(KEY_PRODUCT_TIME, tasklist.getTime());
        values.put(KEY_PRODUCT_AVAILABILITY, tasklist.getIsenable());

        // updating row
        return db.update(Productdetails, values, KEY_PRODUCT_ID + " = ?",
                new String[]{updatingId});
    }


   /* // TO GET A PARTICULAR USER'S DETAILS
    public Clients getContact(String emailid) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(Clientdetails, new String[]{KEY_ID,
                        KEY_NAME, KEY_PH_NO, KEY_ADDRESS}, KEY_ID + "=?",
                new String[]{String.valueOf(emailid)}, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();
        Clients client = new Clients(cursor.getString(0),
                cursor.getString(1), cursor.getString(2), cursor.getString(3));
        return client;
    }*/


  /*  // Updating single contact

*/

   /* // GET THE LIST OF ALL USERS MAINTAINED IN THE DATABASE
    public List<Clients> getListofallClients() {
        List<Clients> clientslist = new ArrayList<Clients>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + Clientdetails;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Clients client = new Clients();
                client.setEmail_id(cursor.getString(0));
                client.setUsers_name(cursor.getString(1));
                client.setPhone_number(cursor.getString(2));
                client.setAddress(cursor.getString(3));
                client.setProduct_id(cursor.getInt(4));
                // Adding contact to list
                clientslist.add(client);
            } while (cursor.moveToNext());
        }
        // return contact list
        return clientslist;
    }*/






}
