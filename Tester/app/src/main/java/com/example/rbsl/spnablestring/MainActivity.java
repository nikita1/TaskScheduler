package com.example.rbsl.spnablestring;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.BackgroundColorSpan;
import android.text.style.ForegroundColorSpan;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        TextView colorText2 = (TextView)findViewById(R.id.colortext2);
        SpannableString text2 = new SpannableString("android-coding.blogspot.com");
        text2.setSpan(new ForegroundColorSpan(Color.RED), 0, 14, 0);
        text2.setSpan(new ForegroundColorSpan(Color.GREEN), 6, 11, 0);
        text2.setSpan(new ForegroundColorSpan(Color.BLUE), 15, text2.length(), 0);
        colorText2.setText(text2, TextView.BufferType.SPANNABLE);
        TextView colorText3 = (TextView)findViewById(R.id.colortext3);
        SpannableString text3 = new SpannableString("android-coding.blogspot.com");
        text3.setSpan(new BackgroundColorSpan(Color.LTGRAY), 0, text3.length(), 0);
        text3.setSpan(new ForegroundColorSpan(Color.RED), 0, 14, 0);
        text3.setSpan(new ForegroundColorSpan(Color.GREEN), 6, 11, 0);
        text3.setSpan(new ForegroundColorSpan(0xFF0000FF), 14, 23, 0);
        text3.setSpan(new ForegroundColorSpan(0x500000FF), 23, text3.length(), 0);
        colorText3.setText(text3, TextView.BufferType.SPANNABLE);
    }
}
