package com.example.rbsl.spnablestring;

import java.io.Serializable;

/**
 * Created by rbsl on 22/8/17.
 */

public class Tasklist implements Serializable {
    String  Task_name;
    Boolean isenable;
    String time;
    int id;
    public Tasklist() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Tasklist(String task_name, Boolean isenable, String time) {

        Task_name = task_name;
        this.isenable = isenable;
        this.time = time;
    }
    public String getTask_name() {
        return Task_name;
    }

    public void setTask_name(String task_name) {
        Task_name = task_name;
    }

    public Boolean getIsenable() {
        return isenable;
    }

    public void setIsenable(Boolean isenable) {
        this.isenable = isenable;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }



}
